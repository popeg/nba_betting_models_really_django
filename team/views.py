from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response

from exceptions.models_exceptions import CannotGetObjectException, ObjectsAreMissingException
from team.api.serializers import BasicTeamSerializer
from team.models import Team


class BasicTeamView(APIView):
    pass


class AllTeamsView(APIView):
    queryset = Team.objects.all()
    serializer_class = BasicTeamSerializer

    def get(self, request, format=None):
        teams = Team.objects.all()
        serializer = BasicTeamSerializer(teams, many=True)
        return Response(serializer.data)


class GetObjectMixin:
    def get_object(self, model, pk):
        result = None
        try:
            result = model.objects.get(pk=pk)
        except Exception:
            raise CannotGetObjectException(
                'Cannot get object from model {}. It seems like it does not exist!'
            )
        finally:
            return result

    def get_all_objects(self, model):
        result = []
        try:
            result = list(model.objects.all())
        except Exception:
            raise ObjectsAreMissingException('No objects retrieved')
        finally:
            return result

    def delete(self, request, pk, format=None):
        instance = self.get_object(Team, pk)
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
