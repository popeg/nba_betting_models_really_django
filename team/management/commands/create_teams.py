"""
"""
import logging, os

from django.core.files.storage import FileSystemStorage
from django.core.management.base import BaseCommand

from constants.strategy_constants import ALL_TEAMS, MAPPING_IMPORT_COMMANDS
from team.models import Team
from untitled.settings import LOGOS_PATH

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Import all teams into database'

    def __create_single_team_object(self):
        pass

    def __create_teams(self):
        for team_command in MAPPING_IMPORT_COMMANDS:
            team = MAPPING_IMPORT_COMMANDS.get(team_command)
            logo = self.get_logo_for_team(team_command)
            team = Team(name=team.get('name'), alias=team.get('alias'), description='something', logo=None)
            team.save()

    def handle(self, *args, **options):
        self.__create_teams()

    @staticmethod
    def get_logo_for_team(team_image_name, extension='.gif'):
        file_name = team_image_name.lower() + extension

        fs = FileSystemStorage()
        disc_file_path = os.path.join(LOGOS_PATH, file_name)
        with open(disc_file_path, mode='rb') as opened_file:
            logo = fs.save(file_name, opened_file)

        return logo
