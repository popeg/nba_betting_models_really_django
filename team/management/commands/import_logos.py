"""
"""
import logging, os
from django.core.files.storage import FileSystemStorage
from django.core.management.base import BaseCommand, CommandError

from constants.strategy_constants import MAPPING_IMPORT_COMMANDS
from team.models import Team
from untitled.settings import LOGOS_PATH


logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Import the team logos or single logo'

    def add_arguments(self, parser):
        parser.add_argument('--teams', type=str)

    @staticmethod
    def modify_logo_in_team(team_filename, team_alias, extension='.gif'):
        changing_team = Team.objects.filter(alias=team_alias).first()

        if not changing_team:
            return

        file_name = team_filename.lower() + extension

        fs = FileSystemStorage()
        disc_file_path = os.path.join(LOGOS_PATH, file_name)
        with open(disc_file_path, mode='rb') as opened_file:
            changing_team.logo = fs.save(file_name, opened_file)

            changing_team.save()

    def __match_teams_with_existing_format(self, team=None):
        if not team:
            raise CommandError("No team(s) was/were provided.")

        team_filename = MAPPING_IMPORT_COMMANDS.get(team)
        if not team_filename:
            raise CommandError("Team {} not found.".format(team_filename))
        self.modify_logo_in_team(team, team_filename['alias'])

    @staticmethod
    def parse_arguments(argument, delimiter=','):
        return argument.split(delimiter)

    def handle(self, *args, **options):
        teams = options.get('teams')
        parsed = list(MAPPING_IMPORT_COMMANDS.keys())
        if teams:
            parsed = self.parse_arguments(teams)

        for team in parsed:
            self.__match_teams_with_existing_format(team)
