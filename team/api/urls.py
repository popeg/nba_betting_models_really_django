"""
Defines urls for API in application
"""

from django.conf.urls import url
from django.contrib import admin

from team.views import BasicTeamView, AllTeamsView

urlpatterns = [
    url(r'^$', AllTeamsView.as_view(), name='list'),
    url(r'^team/', AllTeamsView.as_view()),
    url(r'^team/(?P<id>[\w-]+)/',  admin.site.urls, name='team-detail'),
]
