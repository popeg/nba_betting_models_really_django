"""
Create serializers for Team model
"""
from rest_framework import serializers

from team.models import Team

__author__ = "Przemek"


class BasicTeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = '__all__'
