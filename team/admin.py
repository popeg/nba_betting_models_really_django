import logging
from django.contrib import admin

from constants.admin_constance import (
    DELETE_ALL_TEAMS_DESCRIPTION, DELETE_SELECTED_TEAMS_DESCRIPTION, REGISTER_TEAM_ADMIN,
    CREATE_ALL_TEAMS_FROM_SCRATCH
)
from team.models import Team


logger = logging.getLogger(__name__)


class BaseTeamAdmin(admin.ModelAdmin):
    actions = ['delete_all_teams', 'create_all_teams', 'delete_selected_team']

    non_select_admin_methods_allowed = ('delete_all_teams', 'create_all_teams')

    def _delete_all_teams(self, request, queryset):
        for team in Team.objects.all():
            team.delete()

    def _delete_selected_teams(self, request, queryset):
        for team in queryset:
            team.delete()

    def changelist_view(self, request, extra_context=None):
        """ Allow to delete all teams without selecting """
        if 'action' in request.POST and request.POST['action'] in self.non_select_admin_methods_allowed:
            if not request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                for u in Team.objects.all():
                    post.update({admin.ACTION_CHECKBOX_NAME: str(u.id)})
                request._set_post(post)
        return super(BaseTeamAdmin, self).changelist_view(request, extra_context)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if request.user.username[0].upper() != 'J':
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions


class TeamAdmin(BaseTeamAdmin):
    list_display = ('name', 'alias', 'image_tag', 'last_four_games')
    readonly_fields = ['image_tag']

    class Meta:
        model = Team

    def last_four_games(self, obj):
        # TODO - implement last four games for each team
        return 'Calculated last four games...'

    def delete_all_teams(self, request, queryset):
        return self._delete_all_teams(request, queryset)

    def create_all_teams(self, request, queryset):
        Team.objects.create_teams()

    def delete_selected_team(self, request, queryset):
        return self._delete_selected_teams(request, queryset)

    last_four_games.short_description = 'Calculated last four games...'
    delete_all_teams.short_description = DELETE_ALL_TEAMS_DESCRIPTION
    create_all_teams.short_description = CREATE_ALL_TEAMS_FROM_SCRATCH
    delete_selected_team.short_description = DELETE_SELECTED_TEAMS_DESCRIPTION


if REGISTER_TEAM_ADMIN:
    admin.site.register(Team, TeamAdmin)
