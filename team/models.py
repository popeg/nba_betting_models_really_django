from django.db import models
from django.utils.safestring import mark_safe

from constants.strategy_constants import ALL_TEAMS, BLACKLISTED_TEAMS_ALIASES


class TeamManager(models.Manager):
    def _create_team(self, team_name, team_alias):
        kwargs = {
            'name': team_name,
            'description': 'something',
            'alias': team_alias,
        }
        return self.create(**kwargs)

    def create_teams(self):
        for team in ALL_TEAMS:
            for alias in team:
                team_name = team[alias]
                team_alias = alias
                self._create_team(team_name, team_alias)


class Team(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50, unique=True, editable=False)
    description = models.TextField()
    alias = models.CharField(max_length=5)
    logo = models.ImageField(null=True, blank=True, upload_to='logos', default='')
    date_of_creation = models.DateTimeField(auto_now=False, auto_now_add=True)
    date_of_update = models.DateTimeField(auto_now=True, auto_now_add=False)

    objects = TeamManager()

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.__str__()

    @staticmethod
    def get_all_quarters_score_for_single_team(team):
        team_stats = team.team_stats
        q1, q2, q3, q4 = team_stats[7: 11]
        return q1, q2, q3, q4

    @property
    def is_blacklisted(self):
        return True if self.alias in BLACKLISTED_TEAMS_ALIASES else False

    def image_tag(self):
        if self.logo:
            return mark_safe('<img src="%s" style="width: 45px; height:45px;" />' % self.logo.url)
        else:
            return ''

    image_tag.short_description = 'Team logo'
