"""
Utilities for python betting nba analyzer
"""
from datetime import timedelta, datetime

from constants.constance import DESIRABLE_DATE_API_FORMAT


def get_starting_date_from_day_offset(starting_date, lookup_offset=365):
    result = starting_date - timedelta(days=lookup_offset)  # date - days
    return result


def convert_date_to_str_api_format(date_to_convert, date_format=None):
    if not date_format:
        date_format = DESIRABLE_DATE_API_FORMAT
    return date_to_convert.strftime(date_format)


def convert_date_to_proper_format(date_to_convert, date_format=None):
    if not date_format:
        date_format = DESIRABLE_DATE_API_FORMAT
    return datetime.strptime(date_to_convert, date_format)


def get_day_month_year_from_date(date):
    date_ = date.split('/')
    return [int(date_elem) for date_elem in date_]


def get_all_dates_in_range(starting_date, end_date=None, day_range=None):
    if not end_date and day_range:
        end_date = get_starting_date_from_day_offset(starting_date, day_range)

    offset = starting_date - end_date
    return [end_date + timedelta(days=offset) for offset in range(offset.days + 1)]


def get_normalized_date_for_file(date_to_be_formatted, separator='-'):
    file_date = date_to_be_formatted.split('/')
    return separator.join(file_date)


def get_dates_in_api_format(dates_to_look_into, date_format):
    return [convert_date_to_str_api_format(date_to_convert, date_format) for date_to_convert in dates_to_look_into]


def get_start_date_from_scoreboard(score_board):
    score_board_params = score_board.__dict__
    score_board_params_copy = score_board_params.copy()
    return score_board_params_copy.get('_game_date')
