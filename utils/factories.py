"""
Factories for tests
"""
from datetime import datetime, timedelta

import factory
from factory import fuzzy
from factory.django import ImageField

from game.models import Game, Quarter, Overtime
from team.models import Team

__author__ = "Przemek"


class TeamFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Team
        django_get_or_create = ('name',)

    name = factory.Sequence(lambda number: '--Test-team--{}'.format(number))
    description = 'Dummy data'
    alias = 'ELO'
    logo = ImageField()
    date_of_creation = datetime.now().date()
    date_of_update = datetime.now().date()


class QuarterFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Quarter

    home_team = TeamFactory.create()
    away_team = TeamFactory.create()
    home_team_score = 23
    away_team_score = 34


class OvertimeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Overtime

    first_overtime = QuarterFactory.create()
    second_overtime = QuarterFactory.create()
    third_overtime = QuarterFactory.create()
    fourth_overtime = QuarterFactory.create()
    fifth_overtime = QuarterFactory.create()
    sixth_overtime = QuarterFactory.create()


class GameFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Game
    game_date = fuzzy.FuzzyDate(
        start_date=datetime(2003, 2, 1).date(),
        end_date=datetime.today().date() + timedelta(days=4*52*7+5)
    )
    quarter_one = QuarterFactory.create()
    quarter_two = QuarterFactory.create()
    quarter_three = QuarterFactory.create()
    quarter_four = QuarterFactory.create()

    home_team = TeamFactory.create()
    away_team = TeamFactory.create()
    overtime = OvertimeFactory.create()
