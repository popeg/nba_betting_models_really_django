"""
"""
import os

from constants.constance import FILE_FORMAT
from untitled.settings import API_RESULTS_PATH

__author__ = "Przemek"


def write_to_file(results):
    with open('results_api.txt', mode='w+') as file:
        for line in results:
            file.writelines(line)


class FileWriter:
    def __init__(self, saving_folder_path=API_RESULTS_PATH):
        self.results_folder_path = saving_folder_path
        os.chdir(self.results_folder_path)

    def write_single_game_day_to_file(self, game_day, default_format='csv'):
        file_date = game_day.date
        file_name = FILE_FORMAT.format(file_date)
        file_with_format = '{}.{}'.format(file_name, default_format)
        full_file_path = os.path.join(self.results_folder_path, file_with_format)
        with open(full_file_path, 'w') as file:
            for game in game_day.games:
                file.writelines(game.__dict__.values())

    def write_multiple_game_days_to_file(self, game_days_list, default_format='csv'):
        for game_day in game_days_list:
            self.write_single_game_day_to_file(game_day, default_format)
