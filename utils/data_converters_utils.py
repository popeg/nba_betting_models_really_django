"""
Data parsers and formatting
"""
from utils.date_utils import get_normalized_date_for_file


def flatten_games_structure(games):
    games_new = [[*item] for item in games]
    return games_new


def get_data_to_be_saved_in_file(search_result):
    """Proper format to be saved in output file"""
    game_days = []
    for game_day in search_result:
        game_day.date = get_normalized_date_for_file(game_day.date)
        game_days.append(game_day)
    return game_days
