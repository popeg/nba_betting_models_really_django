from django.db.models import Count, Max


def delete_duplicated_teams_by_name(app_schema, model_cls):
    unique_fields = ['name']
    model_cls = app_schema.get_model('team', 'Team')
    duplicates = (
        model_cls.objects.values(*unique_fields)
        .order_by()
        .annotate(max_id=Max('id'), count_id=Count('id'))
        .filter(count_id__gt=1)
    )

    for duplicate in duplicates:
        (
            model_cls.objects
            .filter(**{x: duplicate[x] for x in unique_fields})
            .exclude(id=duplicate['max_id'])
            .delete()
        )
