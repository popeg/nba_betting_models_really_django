"""
Define application own exception
"""

__author__ = "Przemek"


class NonGameFoundException(Exception):
    """Raise when odds could not be get, no games found"""
    pass
