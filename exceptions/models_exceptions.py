class CannotGetObjectException(Exception):
    pass


class ObjectsAreMissingException(Exception):
    pass
