REGISTER_TEAM_ADMIN = True
REGISTER_GAME_ADMIN = True

DELETE_SELECTED_TEAMS_DESCRIPTION = 'Delete only selected teams'
DELETE_ALL_TEAMS_DESCRIPTION = 'Delete all teams'
CREATE_ALL_TEAMS_FROM_SCRATCH = 'Create all teams from scratch'

DELETE_SELECTED_GAMES = 'Delete only selected game'
DELETE_ALL_GAMES = 'Delete all games in system'
CREATE_ALL_GAMES_FROM_SCRATCH = 'Create all games from the beginning'
