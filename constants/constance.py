import os

DESIRABLE_DATE_API_FORMAT = '%m/%d/%Y'
FILE_FORMAT = 'games_for_{}'
SINGLE_QUARTER_FORMAT = '{}-{}'

EXPECTED_TEAM_FORMAT = {'team1': None,
                        'team2': None,
                        'q1': 1,
                        'q2': 1,
                        'q3': 1,
                        'q4': 1}

SINGLE_GAME_FORMAT = """
Is correct: {}\n
{}: {} vs {} Score: {}\n
q1: {} q2: {} q3: {} q4: {}
"""

EXTENDED_GAME_FORMAT = """
ot1: {} ot2: {} ot3: {} ot4: {} ot5: {} ot6: {}
"""

VALID_GAME_DAY_FORMAT = 'Won {} of {} games in {}. Winning percentage: {}'
VALID_GAME_DAY_WITH_EXCLUDED_FORMAT = 'Won {} of {} games in {}. Winning percentage: {}. Excluded {} games.'
INVALID_GAME_DAY_FORMAT = 'There are no games for {}'
RESULT_FORMAT = 'Search gave winning percentage={}%. Won {} of {}.'
GAME_ODD_MODEL_FORMAT = 'Average odd for {}={}, for {}={}'
BOOKMAKER_MODEL_FORMAT = 'Bookmaker {} have such odds. \nHome team: {}\nAway team: {}'

ODDS_API_KEY = '34eb000274e5b7111b04f2a93e37a4c1'
REQUEST_URL_FORMAT = 'https://api.the-odds-api.com/v2/odds/'

GAME_API_CALL_CACHE_SIZE = 60 * 60 * 24  # by default - 24 hours - one call per day for all games

STRATEGY_RESULT_FORMAT = {
    'games_excluded_number': None,
    'games_won_number':      None,
    'games_total_number':    None,
    'winning_percentage':    None,
    'games_excluded':        None,
    'games_won':             None,
}
