Important information:
If api calls are not working, simply replace:
    HEADERS = {'user-agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_5) '
                              'AppleWebKit/537.36 (KHTML, like Gecko) '
                              'Chrome/45.0.2454.101 Safari/537.36'),
              }
with:
    HEADERS = { 'user-agent': ('customheader')}

in your local nba_py/__init__.py module.

It will use the modified version in the future, but not at this moment:)

TEAMS IMPORT
    python manage.py create_teams

LOGOS IMPORT
    python manage.py import_logos

LOGOS
    To get team's logo, simply type:
        http://127.0.0.1:8000/media/logos/{team_name}.{extension}
        # e.g. http://127.0.0.1:8000/media/logos/atlanta.gif

GAMES
    You have to be logged in to retrieve games.
    Last day games (so called today) - go to:
        http://127.0.0.1:8000/game/
    or
        http://127.0.0.1:8000/

    Dates are accepted in such formats:
    With year:
        1. 12-11-2018
        2. 01-11-2018
        3. 1-11-2018
        4. 01-01-2018

    Without:
        1. 01-01
        2. 1-10
        3. 10-05
        4. 10-5

    Get game for single date (if result no present - nba api call will be made)
        http://127.0.0.1:8000/game/{day}-{month}/
        # e.g. http://127.0.0.1:8000/game/12-11/

    Fill games with data (by default - last 1 day if no argument provided)
        http://127.0.0.1:8000/game/fill-games/{offset}
        # e.g. http://127.0.0.1:8000/game/fill-games/10 - will fill the games for last 10 days starting from today
