import datetime
import logging

from constants.strategy_constants import TEAM_ALIASES
from game.parsers import GamesApiParse
from team.models import Team

from django.contrib.auth.mixins import LoginRequiredMixin
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.generics import ListCreateAPIView, RetrieveAPIView
from game.api.serializers import BasicQuarterSerializer, GameSerializer, GameDaySerializer
from game.models import Quarter, Game, Overtime
from datetime import timedelta

logger = logging.getLogger(__name__)


# TODO - correct logging according to proper standards
# TODO - https://fangpenlin.com/posts/2012/08/26/good-logging-practice-in-python/ -


class OvertimeParser:
    def _parse_overtime(self, game):
        home_overtime_indexes = game[0][11: 21]
        away_overtime_indexes = game[1][11: 21]
        if not (home_overtime_indexes or away_overtime_indexes):
            return []

        pairs = list(zip(home_overtime_indexes, away_overtime_indexes))
        filtered_pairs = [pair for pair in pairs if pair[0] and pair[0] != pair[1]]

        result = [[] for pair in filtered_pairs]

        for i, quarter in enumerate(filtered_pairs):
            result[i] = [quarter[0], quarter[1]]
        return result


class BaseGamesApiMixin(OvertimeParser):
    def __create_quarters(self, game, home_team, away_team):
        quarters = []
        overtime = None

        overtime_score = self._parse_overtime(game)
        home_quarter_indexes = game[0][7: 11]
        away_quarter_indexes = game[1][7: 11]

        for i, home_team_score in enumerate(home_quarter_indexes):
            away_team_score = away_quarter_indexes[i]
            quarter = Quarter.objects.get_or_create(
                quarter_number=i + 1, home_team=home_team, away_team=away_team,
                home_team_score=home_team_score, away_team_score=away_team_score)

            quarters.append(quarter)

        mapping = {'o1': None, 'o2': None, 'o3': None, 'o4': None, 'o5': None, 'o6': None}
        if overtime_score:
            for i, key in enumerate(mapping):
                home_overtime_score = 0
                away_overtime_score = 0
                if i >= len(overtime_score):
                    break
                ot = Quarter.objects.get_or_create(
                    quarter_number=i + 1, home_team=home_team, away_team=away_team,
                    home_team_score=overtime_score[i][0], away_team_score=overtime_score[i][1]
                )
                mapping[key] = ot

        # if i > 3:
        o1 = mapping.get('o1')
        if o1:
            o1 = o1[0]
        o2 = mapping.get('o2')
        if o2:
            o2 = o2[0].__dict__.get('id')
        o3 = mapping.get('o3')
        if o3:
            o3 = o3[0].__dict__.get('id')
        o4 = mapping.get('o4')
        if o4:
            o4 = o4[0].__dict__.get('id')
        o5 = mapping.get('o5')
        if o5:
            o5 = o5[0].__dict__.get('id')
        o6 = mapping.get('o6')
        if o6:
            o6 = o6[0].__dict__.get('id')

        overtime = Overtime.objects.get_or_create(
            first_overtime_id=o1.id if o1 else None, second_overtime_id=o2.id if o2 else None,
            sixth_overtime_id=o3.id if o3 else None, fifth_overtime_id=o5.id if o5 else None,
            fourth_overtime_id=o4.id if o4 else None, third_overtime_id=o3.id if o4 else None
        )
        quarters.append(overtime)
        return quarters

    def __create_single_game_based_on_api(self, game, game_date):
        home_team_alias = game[0][4]
        away_team_alias = game[1][4]
        if any(team_alias not in TEAM_ALIASES for team_alias in (home_team_alias, away_team_alias)):
            return
        home_team = Team.objects.get(alias=home_team_alias)
        away_team = Team.objects.get(alias=away_team_alias)

        q1, q2, q3, q4, overtime = self.__create_quarters(game, home_team, away_team)

        Game.objects.get_or_create(
            game_date=game_date, quarter_one_id=q1[0].id, quarter_two_id=q2[0].id, quarter_three_id=q3[0].id,
            quarter_four_id=q4[0].id, home_team=home_team, away_team=away_team, overtime_id=overtime[0].id
        )

    def create_all_games_within_one_day(self, all_games_in_single_day, game_date):
        for game in all_games_in_single_day:
            self.__create_single_game_based_on_api(game, game_date)


class GamesApiMixin(BaseGamesApiMixin, GamesApiParse):
    """ Class for making calls to nba api """

    def create_games_for_one_day(self, games_date):
        all_games_for_single_day = self.parse_games_for_single_date(games_date)
        self.create_all_games_within_one_day(all_games_for_single_day, games_date)

    def get_all_games_for_one_day(self, date_of_game=None):
        """  By default - get today's game from db """
        if not date_of_game:
            game_datetime = datetime.datetime.today() - timedelta(days=1)
            game_date = game_datetime.date()
        else:
            game_date = self.parse_date_from_url(date_of_game)

        games_in_one_day = Game.objects.filter(game_date=game_date)
        if not games_in_one_day or games_in_one_day:
            self.create_games_for_one_day(game_date)
            games_in_one_day = Game.objects.filter(game_date=game_date)
            if not games_in_one_day:
                logging.info('%s Seems like there is no games for today', game_date)
        return games_in_one_day


class BasicQuarterView(APIView):
    serializer_class = BasicQuarterSerializer

    def get(self, request):
        quarters = Quarter.objects.all()
        serializer = self.serializer_class(quarters, many=True)
        return Response(serializer.data)


class BasicGameView(LoginRequiredMixin, GamesApiMixin, ListCreateAPIView):
    serializer_class = GameSerializer
    queryset = Game.objects.all()

    # @method_decorator(cache_page(GAME_API_CALL_CACHE_SIZE))
    def get(self, request, *args, **kwargs):
        games = self.get_all_games_for_one_day()
        serializer = self.serializer_class(games, many=True)
        return Response(serializer.data)


class GameDetailView(RetrieveAPIView, GamesApiMixin):
    serializer_class = GameSerializer
    queryset = Game.objects.all()

    def get(self, request, *args, **kwargs):
        games = self.get_all_games_for_one_day(kwargs.get('game_date'))
        return Response(self.serializer_class(games, many=True).data)


class FillGamesAdminView(LoginRequiredMixin, RetrieveAPIView, GamesApiMixin):
    serializer_class = GameSerializer
    queryset = Game.objects.all()

    def get(self, request, *args, **kwargs):
        starting_date = kwargs.get('starting_date', None)
        offset = kwargs.get('offset', None)
        dates_to_search_for = self.prepare_dates_within_range_in_api_format(
            starting_date=starting_date, offset=offset
        )
        for date in dates_to_search_for:
            games = self.get_all_games_for_one_day(date)

        return Response(self.serializer_class(games, many=True).data)
