import logging
from datetime import datetime

from django.db import models
from django.utils import timezone

from constants.constance import SINGLE_QUARTER_FORMAT, SINGLE_GAME_FORMAT, VALID_GAME_DAY_FORMAT, \
    VALID_GAME_DAY_WITH_EXCLUDED_FORMAT, INVALID_GAME_DAY_FORMAT, STRATEGY_RESULT_FORMAT, EXTENDED_GAME_FORMAT
from constants.strategy_constants import EXPECTED_QUARTER_DIFFERENCE
from team.models import Team

logger = logging.getLogger(__name__)


class DifferenceByQuarterMixin(models.Model):
    class Meta:
        abstract = True

    @property
    def difference_by_quarter(self):
        if not self.quarter_result:
            return 0
        return abs(self.home_team_score - self.away_team_score)


class Quarter(DifferenceByQuarterMixin):
    quarter_number = models.IntegerField(default=1)
    home_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='home_team_quarter', null=True)
    away_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='away_team_quarter', null=True)
    home_team_score = models.IntegerField(default=0)
    away_team_score = models.IntegerField(default=0)

    def save(self, *args, **kwargs):
        for team in (self.home_team, self.away_team):
            team.save()
        super(Quarter, self).save(*args, **kwargs)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return SINGLE_QUARTER_FORMAT.format(self.home_team_score, self.away_team_score)

    @property
    def was_quarter_played(self):
        return True if self.total_points > 0 else False

    @property
    def get_total_points_in_quarter(self):
        if any(team_score is None for team_score in self.quarter_result):
            return 0
        return sum(self.quarter_result)

    @property
    def total_points(self):
        return self.home_team_score + self.away_team_score

    @property
    def quarter_result(self):
        return [self.home_team_score, self.away_team_score]

    @property
    def quarter_winner(self):
        if self.home_team_score > self.away_team_score:
            return self.home_team
        elif self.home_team_score < self.away_team_score:
            return self.away_team
        return None


class Overtime(models.Model):
    id = models.AutoField(primary_key=True)
    first_overtime = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='first_overtime', null=True)
    second_overtime = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='second_overtime', null=True)
    third_overtime = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='third_overtime', null=True)
    fourth_overtime = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='fourth_overtime', null=True)
    fifth_overtime = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='fifth_overtime', null=True)
    sixth_overtime = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='sixth_overtime', null=True)

    @property
    def all_quarters(self):
        quarters = (
            self.first_overtime, self.second_overtime, self.third_overtime,
            self.fourth_overtime, self.fifth_overtime, self.sixth_overtime
        )
        return [quarter for quarter in quarters if quarter and quarter.home_team and quarter.away_team]

    def __get_played_overtime_quarters_results(self, team_to_get_score):
        team_quarters_result = []
        for quarter in self.all_quarters:
            team_score = getattr(quarter, team_to_get_score)

            team_quarters_result.append(team_score)
        return team_quarters_result

    def get_away_team_overtime_quarters_result(self):
        return self.__get_played_overtime_quarters_results('away_team_score')

    def get_home_team_overtime_quarters_result(self):
        return self.__get_played_overtime_quarters_results('home_team_score')


class Game(models.Model):
    """ Model for single game """
    game_date = models.DateTimeField(default=timezone.now)
    quarter_one = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='quarter_one', null=True)
    quarter_two = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='quarter_two', null=True)
    quarter_three = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='quarter_three', null=True)
    quarter_four = models.ForeignKey(Quarter, on_delete=models.CASCADE, related_name='quarter_four', null=True)

    home_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='home_team', null=True)
    away_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='away_team', null=True)
    overtime = models.ForeignKey(Overtime, on_delete=models.CASCADE, related_name='overtime', null=True, default='')

    def save(self, *args, **kwargs):
        for quarter in (self.quarter_one, self.quarter_two, self.quarter_three, self.quarter_four):
            quarter.save()
        for team in (self.away_team, self.home_team):
            team.save()

        super(Game, self).save(*args, **kwargs)

    def calculate_score_for_team(self, team):
        if team == self.home_team:
            quarters = [self.quarter_one.home_team_score, self.quarter_two.home_team_score,
                        self.quarter_three.home_team_score, self.quarter_four.home_team_score]

            if self.overtime:
                overtime_quarters_home = self.overtime.get_home_team_overtime_quarters_result()
                quarters.extend(overtime_quarters_home)

        else:
            quarters = [self.quarter_one.away_team_score, self.quarter_two.away_team_score,
                        self.quarter_three.away_team_score, self.quarter_four.away_team_score]
            if self.overtime:

                overtime_quarters = self.overtime.get_away_team_overtime_quarters_result()
                quarters.extend(overtime_quarters)

        return sum(quarters)

    @property
    def home_team_score(self):
        return self.team_score(self.home_team)

    @property
    def away_team_score(self):
        return self.team_score(self.away_team)

    def team_score(self, team):
        if team not in [self.home_team, self.away_team]:
            return
        return self.calculate_score_for_team(team)

    @property
    def all_quarters(self):
        return [self.quarter_one, self.quarter_two, self.quarter_three, self.quarter_four,
                self.overtime.first_overtime, self.overtime.second_overtime, self.overtime.third_overtime,
                self.overtime.fourth_overtime, self.overtime.fifth_overtime, self.overtime.sixth_overtime]

    def __repr__(self):
        return self.__str__()

    @property
    def is_game_excluded_from_counting(self):
        return any(team.is_blacklisted for team in (self.home_team, self.away_team))

    @property
    def game_winner(self):
        return self.home_team if self.home_team_score > self.away_team_score else self.away_team

    @property
    def is_strategy_correct(self):
        if self.is_game_excluded_from_counting:
            return False  # skip - one or both teams are blacklisted or overtime (self.game_winner in regular)

        is_correct = False

        # jeżeli zwycięzca kwarty jest inny niż zwycięzca meczu
        winner_condition = self.quarter_one.quarter_winner != self.game_winner
        # is_home_team_winner = self.home_team == self.game_winner

        # jeżeli zwycięzca pierwszej kwarty to goście
        is_away_team_first_quarter_winner = self.quarter_one.quarter_winner == self.away_team
        # is_away_team_first_quarter_winner = False

        # jeżeli różnica w pierwszej kwarcie >= 10 punktów
        difference_condition = self.quarter_one.difference_by_quarter >= EXPECTED_QUARTER_DIFFERENCE

        if winner_condition and difference_condition and is_away_team_first_quarter_winner:
            is_correct = True
        if is_correct:
            print()
        return is_correct

    @property
    def final_score(self):
        final = [0, 0]
        for quarter in self.all_quarters:
            if not quarter:
                continue
            final[0] += quarter.quarter_result[0]
            final[1] += quarter.quarter_result[1]
        return final

    def __str__(self, extended=''):
        basic_format = SINGLE_GAME_FORMAT.format(
                                         self.is_strategy_correct,
                                         self.game_date, self.home_team, self.away_team,
                                         self.final_score, self.quarter_one, self.quarter_two,
                                         self.quarter_three, self.quarter_four)
        if self.overtime:
            extended = EXTENDED_GAME_FORMAT.format(*self.all_quarters[4:])
        return '{}{}'.format(basic_format, extended)


class GameDay(models.Model):
    game_date = models.DateTimeField(default=datetime.now())

    def __init__(self, *args, **kwargs):
        super(GameDay, self).__init__(*args, **kwargs)
        self.games = Game.objects.filter(game_date=self.game_date.date())

        self.total_excluded_games_amount = len(self.excluded_games)
        self.total_games_won_amount = len(self.correct_games)
        self.total_games_amount = len(self.games)

    def __str__(self):
        if self.is_game_day_empty:
            return INVALID_GAME_DAY_FORMAT.format(self.game_date)

        if self.total_excluded_games_amount > 0:  # if we have entries excluded
            return VALID_GAME_DAY_WITH_EXCLUDED_FORMAT.format(self.total_games_won_amount,
                                                              self.games.count(),
                                                              self.game_date,
                                                              self.winning_percentage,
                                                              self.total_excluded_games_amount)

        return VALID_GAME_DAY_FORMAT.format(self.total_games_won_amount,
                                            self.games.count(),
                                            self.game_date,
                                            self.winning_percentage)

    @property
    def is_game_day_empty(self):
        """Return True - if there are no games"""
        return True if len(self.games) == 0 else False

    @property
    def excluded_games(self):
        return [game for game in self.games if game.is_game_excluded_from_counting]

    @property
    def correct_games(self):
        return [game for game in self.games if game.is_strategy_correct]

    @property
    def winning_percentage(self):
        return len(self.correct_games) / self.games.count()

    def get_result_for_one_day(self):
        STRATEGY_RESULT_FORMAT.update({
            'games_excluded_number': self.total_excluded_games_amount,
            'games_won_number': self.total_games_won_amount,
            'games_total_number': self.total_games_amount,
            'winning_percentage': '{} / {}: {}'.format(
                self.total_games_won_amount,
                self.total_games_amount,
                self.winning_percentage
            ),
            'games_excluded': self.excluded_games,
            'games_won': self.correct_games,
        })
        return STRATEGY_RESULT_FORMAT
