from datetime import datetime, timedelta
from unittest import mock

from django.test import TestCase
from django.urls import reverse

from game.views import GamesApiMixin
from utils.factories import GameFactory


class GameViewTestCase(TestCase):
    url = reverse('game:all-games')

    def setUp(self):
        self.games = [GameFactory.create(), GameFactory.create(), GameFactory.create()]

    @mock.patch.object(GamesApiMixin, 'get_all_games_for_one_day')
    def test_get_all_games_should_be_called_when_enter_view(self, mock__games_function):
        self.client.login()

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(mock__games_function.call_count, 1)

    @mock.patch.object(GamesApiMixin, 'create_games_for_one_day')
    def test_basic_game_view_should_show_only_todays_games(self, mock__games_function):
        game_date = datetime.today() - timedelta(days=1)
        today_games = GameFactory.create_batch(game_date=game_date.date(), size=10)
        mock__games_function.return_value = today_games + self.games

        self.client.login()

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), len(today_games))
        self.assertTemplateUsed()

    @mock.patch.object(GamesApiMixin, 'get_all_games_for_one_day')
    def test_get_all_games_should_return_302_when_user_not_authenticated(self, mock__games_function):
        self.client.logout()

        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 302)
