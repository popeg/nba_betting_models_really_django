import logging
from django.contrib import admin

from constants.admin_constance import (
     CREATE_ALL_GAMES_FROM_SCRATCH, DELETE_ALL_GAMES, DELETE_SELECTED_GAMES, REGISTER_GAME_ADMIN,
)
from game.models import Game


logger = logging.getLogger(__name__)


class BaseGameAdmin(admin.ModelAdmin):
    actions = ['delete_all_games', 'create_games_in_range', 'delete_selected_game']

    non_select_admin_methods_allowed = ('delete_all_games', 'create_all_games')

    @staticmethod
    def _delete_all_games(request, queryset):
        queryset.delete()

    def _delete_selected_games(self, request, queryset):
        for game in queryset:
            game.delete()

    def changelist_view(self, request, extra_context=None):
        """ Allow to delete all teams without selecting """
        if 'action' in request.POST and request.POST['action'] in self.non_select_admin_methods_allowed:
            if not request.POST.getlist(admin.ACTION_CHECKBOX_NAME):
                post = request.POST.copy()
                for u in Game.objects.all():
                    post.update({admin.ACTION_CHECKBOX_NAME: str(u.id)})
                request._set_post(post)
        return super(BaseGameAdmin, self).changelist_view(request, extra_context)

    def get_actions(self, request):
        actions = super().get_actions(request)
        if request.user.username[0].upper() != 'J':
            if 'delete_selected' in actions:
                del actions['delete_selected']
        return actions


class GameAdmin(BaseGameAdmin):
    list_display = ('game_date', 'home_team_score', 'away_team_score')

    class Meta:
        model = Game

    def last_four_games(self, obj):
        # TODO - implement last four games for each team
        return 'Calculated last four games...'

    def delete_all_games(self, request, queryset):
        return self._delete_all_games(request, queryset)

    def create_games_in_range(self, request, queryset):
        raise NotImplementedError

    def delete_selected_game(self, request, queryset):
        raise NotImplementedError

    def has_change_permission(self, request, obj=None):
        return False

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        if request.user.is_superuser:
            return qs
        return qs.filter(author=request.user)

    # def get_changeform_initial_data(self, request):

    last_four_games.short_description = 'Calculated last four games...'
    delete_all_games.short_description = DELETE_ALL_GAMES
    create_games_in_range.short_description = CREATE_ALL_GAMES_FROM_SCRATCH
    delete_selected_game.short_description = DELETE_SELECTED_GAMES


if REGISTER_GAME_ADMIN:
    admin.site.register(Game, GameAdmin)
