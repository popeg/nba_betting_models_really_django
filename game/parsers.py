"""
Api parsers - collects calls from nba api
"""
import datetime

from nba_py import Scoreboard


class ParseUrlParameters:
    @staticmethod
    def parse_date_from_url(game_date):
        if isinstance(game_date, datetime.date):
            return game_date
        splitted_date = game_date.split('-')
        if len(splitted_date) == 2:
            splitted_date.append(datetime.datetime.today().year)
        day, month, year = splitted_date
        game_datetime = datetime.datetime(int(year), int(month), int(day))
        return game_datetime.date()

    @staticmethod
    def __create_dates_in_given_days_offset(starting_date=None, offset=None):
        if not starting_date:
            starting_date = datetime.datetime.today()
        if not offset:
            offset = 1
        return [(starting_date - datetime.timedelta(days=days_offset)).date() for days_offset in range(1, offset)]

    def prepare_dates_within_range_in_api_format(self, starting_date=None, offset=None):
        if not offset:
            offset = 1
        if not isinstance(offset, int):
            offset = int(offset)
        return self.__create_dates_in_given_days_offset(starting_date=starting_date, offset=offset + 1)


class GamesApiParse(ParseUrlParameters):
    @staticmethod
    def __parse_games_for_one_day_from_api_call(game_date):
        score_board = Scoreboard(
            year=game_date.year,
            month=game_date.month,
            day=game_date.day
        )
        json_file = getattr(score_board, 'json')
        return json_file.get('resultSets')[1].get('rowSet')

    def parse_games_for_single_date(self, games_date):
        all_games_for_day = self.__parse_games_for_one_day_from_api_call(games_date)
        odd, even = [], []
        for i, game in enumerate(all_games_for_day):
            if i % 2 == 0:
                even.append(game)
            else:
                odd.append(game)
        return list(zip(odd, even))
