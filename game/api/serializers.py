"""
Create serializers for Team model
"""
from rest_framework import serializers

from game.models import Quarter, Game, GameDay
from team.models import Team

__author__ = "Przemek"


class BasicQuarterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quarter
        fields = '__all__'


class TeamSerializer(serializers.ModelSerializer):
    class Meta:
        model = Team
        fields = ['name', 'description', 'alias', 'logo']


class GameSerializer(serializers.ModelSerializer):
    home_team = serializers.StringRelatedField()
    away_team = serializers.StringRelatedField()
    quarter_one = serializers.StringRelatedField()
    quarter_two = serializers.StringRelatedField()
    quarter_three = serializers.StringRelatedField()
    quarter_four = serializers.StringRelatedField()
    overtime = serializers.StringRelatedField()
    is_strategy_correct = serializers.SerializerMethodField(method_name='get_strategy_value')
    game_winner = serializers.SerializerMethodField()

    class Meta:
        model = Game
        fields = ['game_date', 'home_team', 'away_team', 'game_winner', 'final_score',
                  'quarter_one', 'quarter_two', 'quarter_three', 'quarter_four',
                  'overtime', 'is_strategy_correct',
                  ]

    @staticmethod
    def get_strategy_value(game):
        return game.is_strategy_correct

    @staticmethod
    def get_game_winner(game):
        return game.game_winner.name


class GameDaySerializer(serializers.ModelSerializer):
    strategy_result = serializers.SerializerMethodField(method_name='get_strategy_value')
    games = GameSerializer(many=True, read_only=True)

    class Meta:
        model = GameDay
        fields = ['games', 'strategy_result']

    @staticmethod
    def get_strategy_value(games_in_one_day):
        game_day = GameDay.objects.get_or_create(game_date=games_in_one_day[0].game_date)

        game_day = game_day[0]
        return game_day.get_result_for_one_day()
