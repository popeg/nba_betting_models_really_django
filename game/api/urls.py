"""
Defines urls for API in application
"""

from django.conf.urls import url

from game.views import BasicQuarterView, GameDetailView, BasicGameView, FillGamesAdminView

game_patterns = [
    url(r'^$', BasicGameView.as_view(), name='all-games'),
    url(r'^fill-games/$', FillGamesAdminView.as_view(), name='all-games'),
    url(r'^fill-games/(?P<offset>[1-9]?[0-9]?[0-9]?[0-9])/$', FillGamesAdminView.as_view(), name='all-games'),
    url(r'^fill-games/$', FillGamesAdminView.as_view(), name='all-games'),
    url(r'^fill-games/(?P<starting_date>[0-9]{2}-?[0-9]{2}-?[0-9]{4})/(?P<offset>[1-9]?[0-9]?[0-9]?[0-9])/$',
        GameDetailView.as_view(), name='all-games'),
    url(r'^(?P<game_date>[0-9]{2}-?[0-9]{2}-?[0-9]{4})/$', GameDetailView.as_view(), name='game-detail'),
    url(r'^(?P<game_date>[0-9]-?[0-9]-?[0-9]{4})/$', GameDetailView.as_view(), name='game-detail'),
    url(r'^(?P<game_date>[0-9]{2}-?[0-9]-?[0-9]{4})/$', GameDetailView.as_view(), name='game-detail'),
    url(r'^(?P<game_date>[0-9]-?[0-9]{2}-?[0-9]{4})/$', GameDetailView.as_view(), name='game-detail'),
    url(r'^(?P<game_date>[0-9]{2}-?[0-9]{2})/$', GameDetailView.as_view(), name='game-detail'),
    url(r'^(?P<game_date>[0-9]-?[0-9]{2})/$', GameDetailView.as_view(), name='game-detail'),
    url(r'^(?P<game_date>[0-9]{2}-?[0-9])/$', GameDetailView.as_view(), name='game-detail'),
    url(r'^(?P<game_date>[0-9]-?[0-9])/$', GameDetailView.as_view(), name='game-detail'),
]

urlpatterns = [

    url(r'^quarters/', BasicQuarterView.as_view()),
    url(r'^quarter/(?P<id>[\w-]+)/',  BasicQuarterView.as_view(), name='team-detail'),
    url(r'^detailed-view/(?P<id>[\w-]+)/',  BasicQuarterView.as_view(), name='detailed-view'),
]

urlpatterns.extend(game_patterns)
